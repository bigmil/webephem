from skyfield import api, almanac

def season(ts,e,year):
    ty0=ts.utc(int(year), 1, 1) # début d'année
    ty1 = ts.utc(int(year), 12, 31) # fin d'année
    t, y = almanac.find_discrete(ty0, ty1, almanac.seasons(e))
    return(t)