"""Minimal flask application."""

from flask import Flask, render_template
from skyfield import api, almanac
from skyfield.nutationlib import iau2000b
from datetime import datetime, timedelta
#from dateutil import tz
from pytz import timezone
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from app import app
from . twilight import sunrise_sunset_civil, sunrise_sunset_nautical, sunrise_sunset_astronomical
from . alt_sun import Alt_sun
from . earth import season

@app.route('/')
@app.route("/index.html")
def index():
    now=datetime.now()
    year=now.strftime("%Y")
    month=now.strftime("%m")
    day=now.strftime("%d")
    hour=now.strftime("%H")
    minute=now.strftime("%M")
    second=now.strftime("%S")
    h0=datetime.strptime(str(year)+str(month)+str(day),'%Y%m%d') # today at 00
    h24=h0+timedelta(days=1) # today at 24

    # initialize timezone
    cet=timezone('CET') # central european time (CEST in summer)

    # initialise skyfield and timezone
    ts=api.load.timescale()
    e=api.load('de421.bsp')
    earth=e['earth']
    #sun=e['sun']
    #moon=e['moon']

    # initialize place
    levaud=api.Topos('46.4782 N', '6.2388 E')
    earthlevaud=earth+api.Topos('46.4782 N', '6.2388 E')

    # calculate seasons
    springs=list()
    summers=list()
    autumns=list()
    winters=list()
    t=season(ts,e,int(year)-1)
    springs.append(t[0].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    summers.append(t[1].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    autumns.append(t[2].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    winters.append(t[3].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    t=season(ts,e,int(year))
    springs.append(t[0].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    summers.append(t[1].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    autumns.append(t[2].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    winters.append(t[3].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    t=season(ts,e,int(year)+1)
    springs.append(t[0].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    summers.append(t[1].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    autumns.append(t[2].astimezone(cet).strftime("%d.%m %H:%M:%S"))
    winters.append(t[3].astimezone(cet).strftime("%d.%m %H:%M:%S"))

    # calculate sunset, sunrise and twilights
    td0=ts.utc(int(year),int(month), int(day), 0, 0, 0)
    td1=ts.utc(int(year),int(month), int(day), 23, 59, 59)
    trs, yrs = almanac.find_discrete(td0, td1, almanac.sunrise_sunset(e, levaud))
    ttc, ytc = almanac.find_discrete(td0, td1, sunrise_sunset_civil(e, levaud))
    ttn, ytn = almanac.find_discrete(td0, td1, sunrise_sunset_nautical(e, levaud))
    tta, yta = almanac.find_discrete(td0, td1, sunrise_sunset_astronomical(e, levaud))

    # calculate max altitude of sun and corresponding time
    altsun=Alt_sun(e, levaud)
    minimum, alt, az, distance = altsun.altaz_sun(td0,td1)
    #print(minimum, alt, az, distance)

    # calculate moon events
    nowts=ts.utc(int(year), int(month), int(day), int(hour), int(minute), int(second))
    frac_illum="{:2.0f}%".format(almanac.fraction_illuminated(e, 'moon', nowts)*100)

    return render_template('index.html',
                           today=h0.strftime('%d.%m.%Y'),
                           springs=springs,
                           summers=summers,
                           autumns=autumns,
                           winters=winters,
                           years=(int(year)-1,int(year),int(year)+1),
                           sunrise=trs[0].astimezone(cet).strftime("%H:%M:%S"),
                           sunset=trs[1].astimezone(cet).strftime("%H:%M:%S"),
                           twilight_civil_rise=ttc[0].astimezone(cet).strftime("%H:%M:%S"),
                           twilight_civil_set=ttc[1].astimezone(cet).strftime("%H:%M:%S"),
                           twilight_nautical_rise=ttn[0].astimezone(cet).strftime("%H:%M:%S"),
                           twilight_nautical_set=ttn[1].astimezone(cet).strftime("%H:%M:%S"),
                           twilight_astronomical_rise=tta[0].astimezone(cet).strftime("%H:%M:%S"),
                           twilight_astronomical_set=tta[1].astimezone(cet).strftime("%H:%M:%S"),
                           moon_frac_illum=frac_illum,
                           timezenitsun=ts.tt_jd(minimum).astimezone(cet).strftime("%H:%M:%S"),
                           altzenithsun="{:.5f}".format(alt.degrees),
                           azzenithsun="{:.5f}".format(az.degrees),
                          )

@app.route('/sungraphic.html')
def sungraphic():
    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure
    from io import BytesIO
    from flask import make_response
    now=datetime.now()
    year=now.strftime("%Y")
    month=now.strftime("%m")
    day=now.strftime("%d")
    hour=now.strftime("%H")
    minute=now.strftime("%M")
    second=now.strftime("%S")
    h0=datetime.strptime(str(year)+str(month)+str(day),'%Y%m%d') # today at 00
    h24=h0+timedelta(days=1) # today at 24

    # initialize timezone
    cet=timezone('CET') # central european time (CEST in summer)

    # initialise skyfield and timezone
    ts=api.load.timescale()
    e=api.load('de421.bsp')
    earth=e['earth']
    sun=e['sun']
    #moon=e['moon']

    # initialize place
    levaud=api.Topos('46.4782 N', '6.2388 E')
    earthlevaud=earth+api.Topos('46.4782 N', '6.2388 E')

    start=ts.tt(int(year),int(month),int(day),0,0,0).tdb #julian date
    end=start+1
    #print(start,end)
    tj=start
    x=list()
    y=list()
    while tj <= end:
        #print(ts.tt_jd(tj).utc_iso(),alt_sun(tj))
        place=earth+api.Topos('46.4782 N', '6.2388 E')
        astro=place.at(ts.tt(jd=tj)).observe(sun)
        app = astro.apparent()
        alt, az, distance = app.altaz()
        #print(time, ': ', alt, ': ', az)
        x.append(ts.tt_jd(tj).astimezone(cet).strftime("%d %H:%M"))
        y.append(alt.degrees)
        tj+=0.01
    ##plt.figure(figsize=(15,10))
    ##plt.plot(x,y)
    ##ax=plt.axes()
    ##ax=plt.gca() # returns the current active axes object instead of creating a new one.
    ##ax.xaxis.set_minor_locator(ticker.MultipleLocator(1))
    ##ax.xaxis.set_major_locator(ticker.MultipleLocator(6))
    ##ax.yaxis.set_minor_locator(ticker.MultipleLocator(1))
    ##ax.yaxis.set_major_locator(ticker.MultipleLocator(5))
    ##plt.xticks(rotation=45)
    ##plt.grid(which='major',axis='x',color='lightgray',linestyle=':',linewidth=1)
    ##plt.grid(which='major',axis='y',color='lightgray',linestyle=':',linewidth=1)
    ##plt.show()

    fig=Figure(figsize=(15,10))
    graph=fig.add_subplot(1,1,1)
    fig.suptitle('Élevation du soleil pour Le Vaud en fonction de l\'heure', fontsize=14)
    graph.plot(x,y)
    graph.grid(True)
    graph.set_xlabel('JJ HH:MM',size=10)
    graph.set_ylabel('Altitude °',size=10)
    graph.set_xticklabels(x,rotation=90,size=6)
    for label in graph.yaxis.get_ticklabels():
        label.set_fontsize(8)
    canvas=FigureCanvas(fig)
    png_output=BytesIO()
    canvas.print_png(png_output)
    response = make_response(png_output.getvalue())
    response.mimetype = 'image/png'

    return response

@app.route('/daytable.html')
def daytable():
    import calendar
    import numpy as np
    import pandas as pd

    # initialise skyfield
    ts=api.load.timescale()
    e=api.load('de421.bsp')
    earth=e['earth']
    sun=e['sun']

    cet = timezone('CET')
    
    # initialize place
    levaud=api.Topos('46.4782 N', '6.2388 E')

    now=datetime.now()
    year=now.strftime("%Y")
    month=now.strftime("%m")
    
    dow, nrday=calendar.monthrange(int(year),int(month))
    table=pd.DataFrame(columns = ['Jour','Lever','Coucher'])
    for day in range(1,nrday+1):
        # calculate sunset, sunrise and twilights
        td0=ts.utc(int(year),int(month), day, 0, 0, 0)
        td1=ts.utc(int(year),int(month), day, 23, 59, 59)
        trs, yrs = almanac.find_discrete(td0, td1, almanac.sunrise_sunset(e, levaud))
        sunrise=trs[0].astimezone(cet).strftime("%H:%M:%S")
        sunset=trs[1].astimezone(cet).strftime("%H:%M:%S")
        #print(day, sunrise, sunset)
        table.loc[day] = [day, sunrise, sunset]
    #print(table)
    return table.to_html(index=False, col_space=100)