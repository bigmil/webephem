from skyfield.nutationlib import iau2000b

"""
see https://github.com/skyfielders/python-skyfield/issues/225
"""

def sunrise_sunset_civil(ephemeris, topos):

    """Build a function of time that returns whether the sun is up for civil twilight.
    see: https://aa.usno.navy.mil/faq/docs/RST_defs.php#twilight
    The function that this returns will expect a single argument that is
    a :class:`~skyfield.timelib.Time` and will return ``True`` if the
    sun is up, else ``False``.
    """
    sun = ephemeris['sun']
    topos_at = (ephemeris['earth'] + topos).at

    def is_sun_up_at(t):
        """Return `True` if the sun has risen by time `t`."""
        t._nutation_angles = iau2000b(t.tt)
        return topos_at(t).observe(sun).apparent().altaz()[0].degrees > -6.0

    is_sun_up_at.rough_period = 0.5  # twice a day
    return is_sun_up_at

def sunrise_sunset_nautical(ephemeris, topos):

    """Build a function of time that returns whether the sun is up for nautical twilight.
    see: https://aa.usno.navy.mil/faq/docs/RST_defs.php#twilight
    The function that this returns will expect a single argument that is
    a :class:`~skyfield.timelib.Time` and will return ``True`` if the
    sun is up, else ``False``.
    """
    sun = ephemeris['sun']
    topos_at = (ephemeris['earth'] + topos).at

    def is_sun_up_at(t):
        """Return `True` if the sun has risen by time `t`."""
        t._nutation_angles = iau2000b(t.tt)
        return topos_at(t).observe(sun).apparent().altaz()[0].degrees > -12.0

    is_sun_up_at.rough_period = 0.5  # twice a day
    return is_sun_up_at

def sunrise_sunset_astronomical(ephemeris, topos):

    """Build a function of time that returns whether the sun is up for astronomical twilight.
    see: https://aa.usno.navy.mil/faq/docs/RST_defs.php#twilight
    The function that this returns will expect a single argument that is
    a :class:`~skyfield.timelib.Time` and will return ``True`` if the
    sun is up, else ``False``.
    """
    sun = ephemeris['sun']
    topos_at = (ephemeris['earth'] + topos).at

    def is_sun_up_at(t):
        """Return `True` if the sun has risen by time `t`."""
        t._nutation_angles = iau2000b(t.tt)
        return topos_at(t).observe(sun).apparent().altaz()[0].degrees > -18.0

    is_sun_up_at.rough_period = 0.5  # twice a day
    return is_sun_up_at