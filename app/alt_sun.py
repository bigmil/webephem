from skyfield import api
from scipy import optimize

class Alt_sun:
    def __init__(self, ephemerisis, place):
        # initialise skyfield
        self.ts=api.load.timescale()
        self.sun=ephemerisis['sun']
        earth=ephemerisis['earth']
        self.earthplace=earth+place

    def f_alt_sun(self, time):
        alt, az, distance = self._altazdist(time)
        return(alt.degrees*(-1)) # multiply by -1 because we have only minimum search and we want the maximum.

    def altaz_sun(self, tbegin, tend):
        lower_bound=tbegin.tdb
        upper_bound=tend.tdb
        minimum = optimize.fminbound(self.f_alt_sun, lower_bound, upper_bound)
        alt, az, distance = self._altazdist(minimum)
        return(minimum, alt, az, distance)

    def _altazdist(self, time):
        astro=self.earthplace.at(self.ts.tt(jd=time)).observe(self.sun)
        app = astro.apparent()
        alt, az, distance = app.altaz()
        return(alt, az, distance)
