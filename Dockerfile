FROM ubuntu:bionic
LABEL Author="Dominique Stussi" Email="d.stussi@swch.name"

# Environment setting
ENV APP_ENVIRONMENT production
# use for flask (in dev env)
#ENV LC_ALL C.UTF-8
#ENV LANG C.UTF-8

#system packages installation
RUN apt-get update && apt-get install -y \
    nginx \
    python3 \
    python3-pip \
    supervisor \
&& rm -rf /var/lib/apt/lists/*

# set WORKDIR
WORKDIR /workdir

# Flask demo application
COPY ./app ./app
COPY ./requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt

# Nginx configuration
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY nginx.conf /etc/nginx/conf.d/nginx.conf

# Supervisor configuration
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY gunicorn.conf /etc/supervisor/conf.d/gunicorn.conf

# Gunicorn installation
RUN pip3 install gunicorn gevent

# Gunicorn default configuration
COPY gunicorn.config.py gunicorn.config.py
COPY wsgi.py wsgi.py

EXPOSE 8000

#CMD ["/bin/bash"]
CMD ["/usr/bin/supervisord"]