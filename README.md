# Docker Image : ubuntu:bionic-python-gunicorn-nginx

This image contains Nginx, Gunicorn, Python3 on top of ubuntu:bionic docker image.
These nginx and gunicorn software are managed with Supervisor.

## Usage

The entry point of your application must be named as **run.py**. Moreover, the instance in that file must be called **app**.
Also, by default the worker class used is Gevent.

You can include a custom Gunicorn configuration file into your application. By default the configuration file must be name as **gunicorn.config.py**

## Dockerfile example

Here is an example of a Dockerfile using that image :

```
FROM matthieugouel/python-gunicorn-nginx:latest
MAINTAINER Matthieu Gouel <matthieu.gouel@gmail.com>

# Copy the application
COPY . /app

# Install application requirements
RUN pip install -U pip
RUN pip install -r requirements.txt
```

There is also a Flask demo application in the *app* folder of this repository.

## Build and run

First, build your image based on your Dockerfile.

```
docker build -t webephem .
```

Then, you can run a container like this :

```
docker run -p 127.0.0.1:8000:8080 webephem
```

And finally access it with curl for instance :

```
curl localhost:8000
```

Tag the image
```
docker tag webephem bigmil/webephem:latest
```

Push the image
```
docker push bigmil/webephem:latest
```
